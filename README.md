Project work Distribution: 

1. Zuo lin:
- Crypto market api data 
- Market api data 
- User Authentication logic
- dockerise and deploy to openshift

2. Saranya:
- User service layer 
- Rest controller layer for user 
- set up db connectino with mysql
- Rudimentary rest api testing
- dockerise and deploy to openshift

3. Jia yi
- User repository layer 
- seeddb
- initial h2 db setup before migration to mysql