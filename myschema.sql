create database spring_demo;
use spring_demo;

CREATE TABLE user (
  username varchar(100) NOT NULL,
  password varchar(100) NOT NULL,
  id double(100,0) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);

INSERT INTO user(username, password)
VALUES ('zlliu', 'zlliupassword');