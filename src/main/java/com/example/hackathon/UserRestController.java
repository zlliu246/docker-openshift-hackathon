package com.example.hackathon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(value = "/users")
@CrossOrigin
public class UserRestController {

    @Autowired
    private UserService service;

    // Create and insert a new user.
    @PostMapping(value="/", consumes={"application/json","application/xml"},
            produces={"application/json","application/xml"})
    public ResponseEntity<User> addUser(@RequestBody User u) {
        service.createUser(u);
        URI uri = URI.create("/" + u.getUserId());
        return ResponseEntity.created(uri).body(u);
    }

    // Update an existing user.
    @PutMapping(value="/{id}", consumes={"application/json","application/xml"})
    public ResponseEntity modifyUser(@PathVariable int id, @RequestBody User u) {
        if (service.getUser(id) == null)
            return ResponseEntity.notFound().build();
        else {
            service.updateUser(id,u);
            return ResponseEntity.ok().build();
        }
    }

    // Get all user.
    @GetMapping(value="/", produces={"application/json","application/xml"})
    public Collection<User> getAllUsers() {
        return service.getAllUsers();
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity deleteUser(@PathVariable long id) {
        if (service.getUser(id) == null)
            return ResponseEntity.notFound().build();
        else {
            service.deleteUser(id);
            return ResponseEntity.ok().build();
        }
    }






}
