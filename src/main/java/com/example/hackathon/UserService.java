package com.example.hackathon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class UserService {
    
    @Autowired
    private UserRepository repository;

    public void doUserDemo() {
        try {
            long userCount = repository.getUserCount();
            System.out.println(userCount);
        } 
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //get a specific user details from the database
    public User getUser(long userId) {
        try {
           return repository.getUser(userId);
        }
        catch (Exception e) {
            System.out.print(e.getMessage());
            return null;

        }
    }

    //get a specific user details from the database
    public Collection<User> getAllUsers() {
        try {
            return repository.getAllUsers();
        }
        catch (Exception e) {
            System.out.print(e.getMessage());
            return null;

        }
    }

    //add a new user to the database
    public void createUser(User u) {
        try {
            repository.insertUser(u);
            System.out.println("Successfully added new user: " + u.getUsername() + "to the database");
        }
        catch (Exception e) {
            System.out.print(e.getMessage());

        }
    }

    public void deleteUser(long id) {
        try {
            User userToDelete = repository.getUser(id);
            repository.deleteUser(userToDelete);
            System.out.println("Successfully deleted existing user: " + id + "from the database");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateUser(long userId, User u) {
        try {
            repository.updateUser(userId, u);
            System.out.println("Successfully updated user: " + userId + "in the database");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
