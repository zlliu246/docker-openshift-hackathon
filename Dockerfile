FROM openjdk:11.0

ADD target/app.jar app.jar

RUN sh -c 'echo spring.datasource.url=jdbc:mysql://mysql1container:3306/spring_demo > application.properties'

RUN sh -c 'echo spring.datasource.username=root    >> application.properties'

RUN sh -c 'echo spring.datasource.password=password >> application.properties'

ENTRYPOINT ["java","-jar","/app.jar"]